using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class ResetScoreText : MonoBehaviour
{
    public TextMeshProUGUI _text;

    public void Start()
    {
        ScoreCounter.instance._text = _text;
        ScoreCounter.instance.Score = 0;
    }
}
