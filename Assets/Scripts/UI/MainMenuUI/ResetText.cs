using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class ResetText : MonoBehaviour
{
    public TextMeshProUGUI _text;
    private void Start()
    {
        Profiles.instance._text = _text;
    }
}
