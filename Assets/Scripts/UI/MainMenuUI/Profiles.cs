﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Profiles : MonoBehaviour
{
    public TextMeshProUGUI _text;
    public string value;
    public static Profiles instance;
   

    [System.Serializable]
    public class Profile
    {
        public string name;
        public int score;
        public Profile(string name1,int score1)
        {
            name = name1;
            score = score1;
        }
    }

    public List<Profile> profiles = new List<Profile>();
    public Profile currentProfile;

    private void OnEnable()
    {
        _text.text = value;
    }
    public void SetText()
    {
     //TODO set text here;
    }
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
       
    }
    private void Start()
    {
        if (profiles.Count == 0)
        {
            CreateProfile("ABC", 10);
        }
    }

    public void CreateProfile(string name1, int score1)
    {
        profiles.Add(new Profile(name1, score1));
        SetCurrentProfile(profiles[profiles.Count - 1]);
        value = name1;
    }

    public void SetCurrentProfile(Profile profile1)
    {
        currentProfile = profile1;
        _text.text = profile1.name;
    }


    public void SortScore()
    {
        for (int i=0; i<=profiles.Count-2;i++)
        {
            for(int j=0;j<=profiles.Count-2;j++)
            {
                if (profiles[j].score < profiles[j + 1].score)
                    {
                        var temp = profiles[j + 1];
                        profiles[j + 1] = profiles[j];
                        profiles[j] = temp;
                    }
            }
        }
    }
}
