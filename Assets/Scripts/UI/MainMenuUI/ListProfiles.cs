﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListProfiles : MonoBehaviour
{
    public List<TextHolders> _textHolders = new List<TextHolders>();

    private void OnEnable()
    {
        foreach(TextHolders textHolder in _textHolders)
        {
            textHolder.AddtoHoldersList();
        }
        AddtoList();
    }

    public void AddtoList()
    {
        for (int i = 0; i < Profiles.instance.profiles.Count; i++)
        {
            _textHolders[i]._text[0].text = Profiles.instance.profiles[i].name;
            _textHolders[i]._text[1].text = Profiles.instance.profiles[i].score.ToString();
            _textHolders[i].profile = Profiles.instance.profiles[i];
        }
    }

}
