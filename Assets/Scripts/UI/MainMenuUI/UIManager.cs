﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{

    public List<GameObject> UIElements = new List<GameObject>();
 
    public void InitiateCreateProfile()
    {
        UIElements[0].SetActive(false);
        UIElements[2].SetActive(true);
    }
    public void InitiateMainMenu()
    {
        UIElements[2].SetActive(false);
        UIElements[0].SetActive(true);
    }
}
