﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CreateProfile : MonoBehaviour
{
    public TMP_InputField inputField;
    public Profiles profile;
    public TextMeshProUGUI _text;

    public void CreateFunction()
    {
        profile.CreateProfile(inputField.text, 0);
        Profiles.instance.SetCurrentProfile(Profiles.instance.profiles[Profiles.instance.profiles.Count-1]);
    }
}
