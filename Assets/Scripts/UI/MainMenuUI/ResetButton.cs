using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ResetButton : MonoBehaviour
{
    public Button _button;
    // Start is called before the first frame update
    void Start()
    {
        _button.onClick.AddListener(TaskonClick);
    }

    // Update is called once per frame
    void TaskonClick()
    {
        SceneLoader.instance.LoadRequiredScene();
    }
}
