﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class GameOverDisplay : MonoBehaviour
{
    public TextMeshProUGUI _text;
    public ProfileDisplay profileDisplay;
    public GameObject content;

    public List<TextHolders> contentHolders = new List<TextHolders>();
    private void Start()
    {
        Profiles.instance.SortScore();
        for(int i=0;i< Profiles.instance.profiles.Count;i++)
        {
            contentHolders[i]._text[0].text = Profiles.instance.profiles[i].name;
            contentHolders[i]._text[1].text = Profiles.instance.profiles[i].score.ToString();
        }
    }
}
