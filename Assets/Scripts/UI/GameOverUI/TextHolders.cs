﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class TextHolders : MonoBehaviour
{
    public List<TextMeshProUGUI> _text = new List<TextMeshProUGUI>();
    public Profiles.Profile profile;
    private void Awake()
    {
        AddtoHoldersList();
    }

    public void AddtoHoldersList()
    {
        foreach (Transform child in transform)
        {
            _text.Add(child.GetComponent<TextMeshProUGUI>());
        }
    }

    public void SetCurrentProfile()
    {
        Profiles.instance.SetCurrentProfile(profile);
    }
}
