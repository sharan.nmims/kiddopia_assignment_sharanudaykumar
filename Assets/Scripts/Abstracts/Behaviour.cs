﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Behaviour : MonoBehaviour
{
    public float speed;
    public float step;
    public bool hit;

    private void FixedUpdate()
    {
        SpeedCalc();
    }
    private void Update()
    {
        Destruction();
    }
    private void SpeedCalc()
    {
        step = speed * Time.deltaTime;
        if(PowerUpControl.instance.boostActivated)
        {
            step = speed * 2 * Time.deltaTime;
        }
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, -6f, transform.position.z), step);
    }
    private void Destruction()
    {
        if (transform.position.y <= -6f)
        {
            Destroy(gameObject);
        }
    }
}
