﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public int health;
    [SerializeField] int damage;
    public static Health instance;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }
    public void TakeDamage()
    {
        if(health==0)
        {
            health -= damage;
            SceneLoader.instance.LoadRequiredScene();
            Destroy(gameObject);
        }
        if(health > 0)
        {
            ShieldBehaviour.instance.ShieldCalc(false, -1);
        }
    }
}
