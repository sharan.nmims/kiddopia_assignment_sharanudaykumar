﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed;
    public float step;
    public bool stopTouch;

    Vector2 startTouchPosition;
    Vector2 currentPosition;
    public Vector2 Distance;
    private void Update()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            startTouchPosition = Input.GetTouch(0).position;
        }
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            currentPosition = Input.GetTouch(0).position;
            Distance = currentPosition - startTouchPosition;
        }
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            MoveMentCalc();
            Distance = Vector2.zero;
        }
        //if (Input.GetMouseButtonDown(0))
        //{
        //    //print(Input.GetTouch(0).position);
        //    startTouchPosition = Input.mousePosition;
        //}
        //if (Input.GetMouseButton(0))
        //{
        //    currentPosition = Input.mousePosition;
        //    Distance = currentPosition - startTouchPosition;
        //    MoveMentCalc();
        //}

        //if(Input.GetMouseButtonUp(0))
        //{

        //}



        //if(Distance <0.5f)
        //{
        //    step = speed * Time.deltaTime;
        //    transform.position = new Vector3(transform.position.x - 2, transform.position.y, transform.position.z);
        //}

        //if (transform.position.x > -2)
        //{

        //    {

        //    }
        //}

        //if(transform.position.x <2)
        //{
        //    if (Input.GetKeyDown(KeyCode.D))
        //    {
        //        step = speed * Time.deltaTime;
        //        transform.position = new Vector3(transform.position.x+2, transform.position.y, transform.position.z);
        //    }
        //}
    }

    private void MoveMentCalc()
    {
        if (Distance.magnitude > 200f)
        {
            if (Distance.x > 0f)
            {
                if (transform.position.x < 2)
                {
                    transform.position = new Vector3(transform.position.x + 2, transform.position.y, transform.position.z);
                }

            }
            else if (Distance.x < 0f)
            {
                if (transform.position.x > -2)
                {
                    transform.position = new Vector3(transform.position.x - 2, transform.position.y, transform.position.z);
                }
            }
            Distance = Vector2.zero;
        }
    }
}
