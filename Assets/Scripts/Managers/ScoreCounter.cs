﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class ScoreCounter : MonoBehaviour
{
    public static ScoreCounter instance;
    public int Score;
    public TextMeshProUGUI _text;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }

        DontDestroyOnLoad(this.gameObject);
    }

    public void AddScore(int score)
    {
        Score += score;
        _text.text = Score.ToString();
        var a = Profiles.instance.profiles;
        a[a.Count - 1].score = Score;
    }
}
