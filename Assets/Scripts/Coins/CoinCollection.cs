﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinCollection : MonoBehaviour
{
    public int value;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            ScoreCounter.instance.AddScore(value);
            SpawnerController.instance.AddCoinCounter(1);
            Destroy(gameObject);
        }
        
    }
}
