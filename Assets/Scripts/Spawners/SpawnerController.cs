﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    public int random;
    public float time;
    public float delay;
    public int coinCounter;
    public int coinCounterEnemy;
    bool isSpawningEnemy;

    public static SpawnerController instance;
   

    public List<SpawnerBehaviour> spawners = new List<SpawnerBehaviour>();
    public List<PowerUpBehaviour> powerUps = new List<PowerUpBehaviour>();
    public List<EnemyBehaviour> enemies = new List<EnemyBehaviour>();

    public int randomNumber;

    private void Awake()
    {
        delay = Random.Range(1f, 1.5f);
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    private void Update()
    {
        time += Time.deltaTime;

        if(time>delay)
        {
            for (int i = 0; i < spawners.Count; i++)
            {
                spawners[i].isActiveSpawner = false;
            }
            randomNumber = UnityEngine.Random.Range(0, 3);
            spawners[randomNumber].isActiveSpawner = true;
            Spawn();
            time = 0f;
            delay = Random.Range(1f, 1.5f);
        }
    }

    private void Spawn()
    {
        SpawnEnemy();
        SpawnCoinorPowerUp();
      
    }
    private void LateUpdate()
    {
        if(isSpawningEnemy)
        { isSpawningEnemy = false; }
    }

    private void SpawnCoinorPowerUp()
    {
        if (!isSpawningEnemy)
        {
            if (coinCounter > 0 && coinCounter >= 25)
            {
                spawners[randomNumber].SpawnPowerUp();
                coinCounter = 0;
            }

            else if (coinCounter > 0 && coinCounter <= 25)
            {
                spawners[randomNumber].SpawnCoin();
            }

            else if (coinCounter == 0)
            {
                spawners[randomNumber].SpawnCoin();
            }
        }
    }


    private void SpawnEnemy()
    {
        if (coinCounterEnemy >= 8f)
        {
            isSpawningEnemy = true;
            int randomEnemy = randomNumber = UnityEngine.Random.Range(0, 2);
            spawners[randomNumber].SpawnEnemy(); 
            coinCounterEnemy = 0;
        }
       
    }
    public void AddCoinCounter(int coinAdder)
    {
        coinCounter += coinAdder;
        coinCounterEnemy += coinAdder;
    }
  
}
