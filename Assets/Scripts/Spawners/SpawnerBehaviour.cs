﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerBehaviour : MonoBehaviour
{
    public CoinBehaviour coins;
    
    public SpawnerController spawnerController;

    public float mainTimer;
    public bool isActiveSpawner;
    public int randomCount;

    private void Update()
    {
        mainTimer += Time.deltaTime;
    }

    public void SpawnPowerUp()
    {
        randomCount = UnityEngine.Random.Range(0, 2);
        Behaviour powerUp = spawnerController.powerUps[randomCount];
        SpawnPowerUporEnemy(powerUp);
    }

    public void SpawnEnemy()
    {
        randomCount = UnityEngine.Random.Range(0, 2);
        Behaviour enemy = spawnerController.enemies[randomCount];
        SpawnPowerUporEnemy(enemy);
    }

    private void SpawnPowerUporEnemy(Behaviour objectToSpawn)
    {
        var temp = Instantiate(objectToSpawn, transform.position, Quaternion.identity);
    }

    public void SpawnCoin()
    {
            randomCount = UnityEngine.Random.Range(2, 5);
            for (int i = 1; i <= randomCount; i++)
            {
                Vector3 newPos = new Vector3(transform.position.x, transform.position.y + i, transform.position.z);
                CoinBehaviour temp = Instantiate(coins, newPos, Quaternion.identity);
            }
    }

}
