﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpControl : MonoBehaviour
{
    public static PowerUpControl instance;
    public bool boostActivated;
    public int boostTimer;
    public bool shieldActivated;
    public int shieldTimer;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    public void DeactivateBoost()
    {
        StopCoroutine("SetBoostFalse");
        StartCoroutine("SetBoostFalse");
    }
    IEnumerator SetBoostFalse()
    {
        yield return new WaitForSeconds(boostTimer);
        boostActivated = false;
    }
    public void DeactivateShield()
    {
        StopCoroutine("SetShieldFalse");
        StartCoroutine("SetShieldFalse");
    }
    IEnumerator SetShieldFalse()
    {
        yield return new WaitForSeconds(shieldTimer);
        ShieldBehaviour.instance.ShieldCalc(false, -1);
        shieldActivated = false;
    }
}
