﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpBehaviour : Behaviour
{
    public int value;
    public bool isBoost;
    public bool isShield;

    [SerializeField] ShieldBehaviour shieldBehaviour;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if(isBoost)
            {
                PowerUpControl.instance.boostActivated = true;
                PowerUpControl.instance.DeactivateBoost();
            }
            if(isShield)
            {
                PowerUpControl.instance.shieldActivated = true;
                ShieldBehaviour.instance.ShieldCalc(true, 1);
                PowerUpControl.instance.DeactivateShield();
            }
            Destroy(gameObject);
        }
    }
}
