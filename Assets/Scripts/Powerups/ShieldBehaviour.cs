﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldBehaviour : MonoBehaviour
{
    public int shieldTimer;
    public GameObject shield;
    public static ShieldBehaviour instance;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }
    public void ShieldCalc(bool setShield, int health)
    {
        shield.gameObject.SetActive(setShield);
        if (Health.instance.health == 0 && health == 1)
        {
            Health.instance.health += health;
        }

        else if(Health.instance.health >=0 && health == -1)
        {
            Health.instance.health += health;
        }
    }
}
